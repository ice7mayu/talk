## 客户端开发分享
陈佳平

---
#### 功能: 对评论进行文字内容审核
![](images/text.jpg)

---
#### 功能

- **对评论进行图片内容审核**
![](images/pic.jpg)

---
#### 开发流程

![](images/process.png)

---
#### 开发环境
- OS: win7 64位
- Python: 3.6.4 64位
- pyinstaller:3.3.1
--- 
#### 技术
- tkinter
- pandas
- PIL
- xlsxwriter
---

### tkinter
---
### [Python Data Analysis Library(pandas)](https://xlsxwriter.readthedocs.io/)
无论是数据分析还是数据挖掘来说，Pandas是一个非常重要的Python包
- 数据处理非常简单
- 数据处理速度大大优化
```md
# 安装
$ pip install pandas
```

---
# [Python Imaging Library (PIL)](https://pillow.readthedocs.org/)：
### Python平台的图像处理标准库，功能非常强大，但API却非常简单易用。

```md
# 安装
$ pip install pillow
```
---
# [XlsxWriter](https://xlsxwriter.readthedocs.io/)
### XlsxWriter是python用来构造xlsx文件的模块，
- 可以向excel2007+中写text，numbers，formulas 公式以及超链接
- 可以完成xlsx文件的自动化构造：合并单元格，制作excel图表等功能
- 写大文件，速度快且只占用很小的内存空间

- **不支持读或者改现有的excel文件**
```md
# 安装
$ pip install XlsxWriter
```

---

## Enjoy writing slides! :+1:

### https://github.com/yhatt/marp

Copyright &copy; 2016 [Yuki Hattori](https://github.com/yhatt)
This software released under the [MIT License](https://github.com/yhatt/marp/blob/master/LICENSE).